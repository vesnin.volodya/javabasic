package sber_examples;

public class Test2 {
    public static void main(String[] args) {
        try {
            p();
            System.out.println("После вызова метода");
        }
        catch (NumberFormatException ex) {
            System.out.println("NumberFormatException");
        }
        catch (RuntimeException ex) {
            System.out.println("RuntimeException");
        }
    }

    static void p() {
        String s = "5.6";
        Integer.parseInt(s); // Приводит к NumberFormatException

        int i = 0;
        int y = 2 / i;
        System.out.println("Welcome to Java");
    }
}
