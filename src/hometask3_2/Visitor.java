package hometask3_2;

import java.util.Random;

public class Visitor {
    private String name;
    private int id;
    private Book book;
    private static int count = 0;

    Visitor(String name) {
        count++;
        this.name = name;
        this.id= count;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }

    public void borrowBook(Book book) {
        this.book = book;
    }

    public void returnBook() {
        this.book = null;
    }

    public boolean hasBook() {
        if (this.book != null) {
            return true;
        } else return false;
    }

    public Book borrowedBook() {
        return book;
    }


}
