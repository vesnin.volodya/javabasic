package hometask3_2;

public class Book {
    private String title;
    private String author;

    private double rating = 0;
    private int markCount = 0;
    private boolean isBorrowed = false;

    Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public void borrowBook() {
        this.isBorrowed = true;
    }

    public void returnBook() {
        this.isBorrowed = false;
    }

    public boolean isBorrowed() {
        return this.isBorrowed;
    }

    public double getRating() {
        return this.rating;
    }

    public void addMark(int mark) {
        markCount++;
        if (markCount == 1) {
            this.rating = mark;
        } else this.rating = (this.rating + mark) / 2.0;
    }


}
