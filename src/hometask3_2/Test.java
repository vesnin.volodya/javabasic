package hometask3_2;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        CoolLibrary coolLibrary = new CoolLibrary();

        // 1 задача
        coolLibrary.addBook("Мартин Иден", "Джек Лондон");
        coolLibrary.addBook("По ком звонит колокол", "Эрнест Хемингуэй");
        coolLibrary.addBook("Обломов", "Иван Гончаров");
        coolLibrary.addBook("Герой нашего времени", "Михаил Лермонтов");
        coolLibrary.addBook("Война и мир", "Лев Толстой");
        coolLibrary.addBook("Преступление и наказание", "Фёдор Достоевский");

        coolLibrary.showAllBooks();

        coolLibrary.addBook("По ком звонит колокол", "Эрнест Хемингуэй");

        // 2 задача
        System.out.println("\n");
        coolLibrary.deleteBook("Обломов");
        coolLibrary.showAllBooks();

        coolLibrary.searchBookByName("По ком звонит колокол");
        coolLibrary.searchBookByName("Обломов");

        coolLibrary.addBook("Анна Каренина", "Лев Толстой");
        ArrayList<Book> books = coolLibrary.searchBookByAuthor("Лев Толстой");
        for (Book book: books
             ) {
            System.out.println(book.getTitle());
        }

        Visitor visitor1 = new Visitor("Владимир");
        System.out.println(visitor1.getId() + " " + visitor1.getName());
        coolLibrary.addVisitors(visitor1);
        coolLibrary.addVisitors("Алексей");

        coolLibrary.showAllVisitors();

        coolLibrary.lendBookToVisitor("Алексей", "Герой нашего времени");
        coolLibrary.lendBookToVisitor("Владимир", "Герой нашего времени");

        coolLibrary.returnBookToLibrary("Алексей", 4);
        coolLibrary.lendBookToVisitor("Владимир", "Герой нашего времени");

        coolLibrary.returnBookToLibrary("Владимир", 5);

        System.out.println(coolLibrary.getRating("Герой нашего времени"));


    }
}
