package hometask3_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class CoolLibrary {

    private ArrayList<Book> books = new ArrayList<>();
    private ArrayList<Visitor> visitors = new ArrayList<>();

    public void addBook(String name, String author) {
        boolean isDuplicate = false;
        for (Book book: books
             ) {
            if (Objects.equals(book.getTitle(), name)) {
                isDuplicate = true;
                System.out.println("Данная книга уже есть в библиотеке!");
                break;
            }
        }
        if (!isDuplicate) {
            books.add(new Book(name, author));
        }
    }

    public void addBook(Book newBook) {
        boolean isDuplicate = false;
        for (Book book: books
        ) {
            if (Objects.equals(book.getTitle(), newBook.getTitle())) {
                isDuplicate = true;
                System.out.println("Данная книга уже есть в библиотеке!");
                break;
            }
        }
        if (!isDuplicate) {
            books.add(newBook);
        }
    }

    public void showAllBooks() {
        for (Book book: books
             ) {
            System.out.println(book.getAuthor() + " \"" + book.getTitle() +"\"");
        }
    }

    public void deleteBook(String name) {
        int index = 0;
        for (Book book: books
             ) {

            if (Objects.equals(name.toLowerCase(), book.getTitle().toLowerCase()) && !book.isBorrowed()) {
                books.remove(index);
                break;
            }
            index++;
        }
    }

    public Book searchBookByName(String name) {
        for (Book book: books
             ) {
            if (Objects.equals(name.toLowerCase(), book.getTitle().toLowerCase())) {
                System.out.println("Книга найдена");
                return book;
            }
        }
        System.out.println("Книга не найдена");
        return null;
    }

    public ArrayList<Book> searchBookByAuthor(String author) {
        ArrayList<Book> result = new ArrayList<>();
        for (Book book: books
        ) {
            if (Objects.equals(author.toLowerCase(), book.getAuthor().toLowerCase())) {
                result.add(book);
            }
        }
        return result;
    }

    public void addVisitors(String name) {
        visitors.add(new Visitor(name));
    }

    public void addVisitors(Visitor visitor) {
        visitors.add(visitor);
    }

    public void showAllVisitors() {
        for (Visitor visitor: visitors
        ) {
            System.out.println("id: " + visitor.getId() + " " + visitor.getName());
        }
    }

    public ArrayList<Visitor> getVisitors() {
        return this.visitors;
    }

    public Visitor searchVisitor(String name) {
        for (Visitor visitor: visitors
             ) {
            if (visitor.getName().equals(name)) {
                return visitor;
            }
        }
        System.out.println("посетитель не найден");
        return null;
    }

    public Visitor searchVisitor(String name, int id) {
        for (Visitor visitor: visitors
        ) {
            if (visitor.getName().equals(name) && visitor.getId() == id) {
                return visitor;
            }
        }
        System.out.println("посетитель не найден");
        return null;
    }

    public void lendBookToVisitor(String visitorName, String bookName) {
        Visitor visitor = searchVisitor(visitorName);
        Book book = searchBookByName(bookName);

        if (Objects.equals(book.getTitle().toLowerCase(), bookName.toLowerCase())) {
            if (!book.isBorrowed() && !visitor.hasBook()) {
                visitor.borrowBook(book);
                book.borrowBook();
                System.out.println("Книга одолжена!");
            } else System.out.println("Книгу нельзя взять");

        }
    }

    public void returnBookToLibrary(String visitorName, int mark) {
        Visitor visitor = searchVisitor(visitorName);
        visitor.borrowedBook();
        Book book = visitor.borrowedBook();;

        if (visitor.hasBook() && book.isBorrowed()) {
            visitor.returnBook();
            book.returnBook();
            book.addMark(mark);
            System.out.println("Книга возвращена");
        } else System.out.println("Ошибка возврата");
    }

    public double getRating(String bookName) {
        Book book = searchBookByName(bookName);
        return book.getRating();
    }

}


