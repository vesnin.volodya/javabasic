package hometask1;

import java.util.Scanner;

/*
На вход подается количество секунд, прошедших с начала текущего дня – count.
Выведите в консоль текущее время в формате: часы и минуты.
 */
public class Task4 {
    static int SECONDS_PER_MINUTES = 60;
    static int MINUTES_PER_HOUR = 60;
    static  int HOURS_PER_DAY = 24;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int totalSeconds = scanner.nextInt();

        int totalMinutes = totalSeconds / SECONDS_PER_MINUTES;
        int minutes = totalMinutes % MINUTES_PER_HOUR;
        int totalHours = totalMinutes / MINUTES_PER_HOUR;
        int hours = totalHours % HOURS_PER_DAY;

        System.out.printf("%s %s", hours, minutes);
    }
}
