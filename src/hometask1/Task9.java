package hometask1;

import java.util.Scanner;

/*
На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k тугриков.
Вычислите и выведите, сколько гостей можно пригласить на мероприятие.
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        int guestNumber = n / k;

        System.out.println(guestNumber);
    }
}
