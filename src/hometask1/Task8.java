package hometask1;

import java.util.Scanner;

/*
На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30 дней.
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        double budgetPerDay = (double) n / 30;
        System.out.println(budgetPerDay);
    }
}
