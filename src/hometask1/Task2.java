package hometask1;

import java.util.Scanner;

/*
На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите два целых числа: ");
        System.out.print("a = ");
        int a = scanner.nextInt();

        System.out.print("b = ");
        int b = scanner.nextInt();

        double res = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2.0);

        System.out.printf("Среднее квадратическое a и b равно %s", res);
    }
}
