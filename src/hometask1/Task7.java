package hometask1;

import java.util.Scanner;

/*
На вход подается двузначное число n. Выведите число, полученное перестановкой цифр в исходном числе n.
Если после перестановки получается ведущий 0, его также надо вывести.
 */
public class Task7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int n1 = n / 10;
        int n2 = n % 10;

        System.out.println(n2);
    }
}
