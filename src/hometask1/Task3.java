package hometask1;

import java.util.Scanner;

/*
Прочитайте из консоли имя пользователя и выведите в консоль строку: Привет, <имя пользователя>!
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Как Вас зовут?");
        String userName = scanner.nextLine();

        System.out.println("Привет, " + userName + "!");
    }
}
