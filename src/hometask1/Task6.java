package hometask1;

import java.util.Scanner;

/*
На вход подается количество километров count.
Переведите километры в мили (1 миля = 1,60934 км) и выведите количество миль.
 */
public class Task6 {
    static double KILOMETERS_PER_MILES = 1.60934;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count =  scanner.nextInt();

        double miles = count / KILOMETERS_PER_MILES;

        System.out.println(miles);
    }
}
