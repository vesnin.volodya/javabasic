package hometask1;

/*
Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров).
На вход подается количество дюймов, выведите количество сантиметров.
 */

import java.util.Scanner;

public class Task5 {
    static double CENTIMETERS_PER_INCH = 2.54;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        System.out.print("Введите количество дюймов: ");
        int inch = scanner.nextInt();

        double centimeters = inch * CENTIMETERS_PER_INCH;

        System.out.println(centimeters);
    }
}
