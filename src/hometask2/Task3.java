package hometask2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int hour = sc.nextInt();

        if (hour > 12) {
            System.out.println("Пора");
        } else {
            System.out.println("Рано");
        }
    }
}
