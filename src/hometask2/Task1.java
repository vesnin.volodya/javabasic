package hometask2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int firstMark = sc.nextInt();
        int secondMark = sc.nextInt();
        int thirdMark = sc.nextInt();

        if (firstMark > secondMark && secondMark > thirdMark) {
            System.out.println("Петя, пора трудиться");
        } else {
            System.out.println("Петя молодец!");
        }
    }

}
