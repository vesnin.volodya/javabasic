package hometask2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        double D = Math.pow(b, 2) - 4 * a * c;

        if (D < 0) {
            System.out.println("Решения нет");
        } else {
            System.out.println("Решение есть");
        }
    }
}
