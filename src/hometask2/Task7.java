package hometask2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        System.out.println(s.substring(0,s.indexOf(' ')));
        System.out.println(s.substring(s.indexOf(' ')+1));
    }
}