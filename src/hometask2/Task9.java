package hometask2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int deg = sc.nextInt();

        double res1 = Math.pow(Math.sin(Math.toRadians(deg)), 2);
        double res2 = Math.pow(Math.cos(Math.toRadians(deg)), 2);
        double res = res1 + res2;
        System.out.println(res == 1.0);
    }
}
