package hometask2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double n = sc.nextDouble();

        double res = Math.log(Math.exp(n));
        System.out.println(res == n);
    }
}
