package hometask3;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int res = 0;
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();

        for (int i=m; i<=n; i++) {
            res += i;
        }
        System.out.println(res);
    }
}
