package hometask3;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        for (int i=3; i>=0; i--) {
            int count = n/(int)Math.pow(2,i);
            System.out.println(count);
            n -= count*(int)Math.pow(2,i);
        }
    }
}
