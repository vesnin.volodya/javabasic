package hometask3;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();

        while (m>=n) {
            m -= n;
        }
        System.out.println(m);
    }
}
