package hometask3;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int num;
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        for (int i = 1; i <= n; i++) {
            for (int j = i; j < n; j++) {
                System.out.print(" ");
            }
            for (int j = i; j > 1; j--) {
                System.out.print("#");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int j = 1; j < n; j++) {
            System.out.print(" ");
        }

        System.out.print("|");

    }
}
