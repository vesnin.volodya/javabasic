package hometask3;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int count=0;
        Scanner sc = new Scanner(System.in);

        while (true) {
            int input = sc.nextInt();
            if (input < 0) {
                count += 1;
            } else {
                break;
            }
        }
        System.out.println(count);
    }
}
