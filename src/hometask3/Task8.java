package hometask3;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        int sum=0;
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int p = sc.nextInt();

        for (int i=0; i<n; i++) {
            int a = sc.nextInt();
            if (a>p) {
                sum += a;
            }
        }
        System.out.println(sum);
    }
}
