package hometask3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int res = 0;
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();

        for (int i=1; i<=n; i++) {
            res += Math.pow(m, i);
        }
        System.out.println(res);
    }
}
