package hometask3_1.hometask3_1_6;

public class Test {
    public static void main(String[] args) {
        char[] charArr = {'a', 'm', 'a', 'z', 'e'};
        AmazingString amazingString1 = new AmazingString(charArr);

        String str = "amazing char";
        AmazingString amazingString2 = new AmazingString(str);

        System.out.println(amazingString1.symbolByIndex(3));
        System.out.println(amazingString2.symbolByIndex(2));

        System.out.println(amazingString1.length());
        System.out.println(amazingString2.length());

        char[] charSub = {'m', 'b'};
        System.out.println(amazingString1.isSubstring(charSub));
        System.out.println(amazingString1.isSubstring("maz"));

        amazingString2.deleteSpaces();
        System.out.println(amazingString2.getCharArray());

        amazingString2.reverse();
        System.out.println(amazingString2.getCharArray());

    }
}
