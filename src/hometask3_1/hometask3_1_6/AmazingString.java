package hometask3_1.hometask3_1_6;

public class AmazingString {
    private char[] charArray;

    AmazingString(char[] charArray) {
        this.charArray = charArray;
    }

    AmazingString(String str) {
        this.charArray = str.toCharArray();
    }

    public char[] getCharArray() {
        return charArray;
    }


    public char symbolByIndex(int i) {
        return charArray[i];
    }

    public int length() {
        return charArray.length;
    }

    public boolean isSubstring(char[] subArray) {
        int subArrayLength = subArray.length;
        if (subArrayLength > this.length()) {
            return false;
        }

        int limit = this.length() - subArrayLength;

        for (int i = 0; i < limit+1; i++) {
            boolean subArrayFound = true;
            for (int j = 0; j < subArrayLength; j++) {
                if (subArray[j] != charArray[i+j]) {
                    subArrayFound = false;
                    break;
                }
            }
            if (subArrayFound) return true;
        }
        return false;
    }

    public boolean isSubstring(String subStr) {
        char[] subArray = subStr.toCharArray();
        int subArrayLength = subArray.length;
        if (subArrayLength > this.length()) {
            return false;
        }

        int limit = this.length() - subArrayLength;

        for (int i = 0; i < limit+1; i++) {
            boolean subArrayFound = true;
            for (int j = 0; j < subArrayLength; j++) {
                if (subArray[j] != charArray[i+j]) {
                    subArrayFound = false;
                    break;
                }
            }
            if (subArrayFound) return true;
        }
        return false;
    }

    public void deleteSpaces() {
        int spacesCount = 0;
        for (int i = 0; i < this.length(); i++) {
            if (charArray[i] == ' ') {
                spacesCount++;
            }
        }

        if (spacesCount > 0) {
            char[] newCharArray = new char[this.length() - spacesCount];
            int j = 0;
            for (int i = 0; i < this.length(); i++) {
                if (charArray[i] != ' '){
                    newCharArray[j] = charArray[i];
                    j++;
                }
            }
            this.charArray = newCharArray;
        }
    }

    public void reverse() {
        char[] newCharArray = new char[this.length()];
        int len = this.length();
        for (int i = 0; i < len; i++) {
            newCharArray[len-1-i] = charArray[i];
        }

        charArray = newCharArray;
    }


}
