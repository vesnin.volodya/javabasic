package hometask3_1.hometask3_1_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class StudentService {
    StudentService() {}

    public static Student bestStudent(Student[] students) {
        int numberStudent = students.length;
        int maxIndex = 0;
        double maxMeanGrade = 0;
        for (int i = 0; i < numberStudent; i++) {
            if (maxMeanGrade < students[i].meanGrade()) {
                maxMeanGrade = students[i].meanGrade();
                maxIndex = i;
            }
        }
        return students[maxIndex];
    }

    public static Student[] sortBySurname(Student[] students) {
        Arrays.sort(students, Comparator.comparing(Student::getSurname));
        return students;
    }

}
