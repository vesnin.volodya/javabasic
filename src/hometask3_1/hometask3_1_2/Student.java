package hometask3_1.hometask3_1_2;

public class Student {
    private String name;
    private String surname;
    private int[] grades;

    Student() {}

    Student(String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public int[] getGrades() {
        return this.grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        int len = this.grades.length;
        for (int i = 1; i < len; i++) {
            this.grades[i-1] = this.grades[i];
        }
        this.grades[len-1] = grade;
    }

    public double meanGrade() {
        double meanValue;
        int sum = 0;
        int len = this.grades.length;

        for (int i = 0; i < len; i++) {
            sum += this.grades[i];
        }

        meanValue = sum / (len*1.0);

        return meanValue;
    }

}
