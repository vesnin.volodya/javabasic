package hometask3_1.hometask3_1_2;

import javax.annotation.processing.SupportedSourceVersion;

public class Test {
    public static void main(String[] args) {
        Student student1 = new Student();

        student1.setName("Владимир");
        student1.setSurname("Веснин");

        int[] grades = {5, 3, 4, 4};
        student1.setGrades(grades);

        System.out.println(student1.getName());
        System.out.println(student1.getSurname());


        for (int i = 0; i < student1.getGrades().length; i++) {
            System.out.print(student1.getGrades()[i] + " ");
        }
        System.out.println(" ");

        student1.addGrade(2);

        for (int i = 0; i < student1.getGrades().length; i++) {
            System.out.print(student1.getGrades()[i] + " ");
        }
        System.out.println(" ");

        System.out.println(student1.meanGrade());

        int[] grades2 = {4, 3, 5, 3, 5};
        Student student2 = new Student("Катя", "Перова", grades2);

        int[] grades3 = {3, 3, 4, 3, 5};
        Student student3 = new Student("Вася", "Кудрин", grades3);

        int[] grades4 = {4, 4, 5, 5, 5};
        Student student4 = new Student("Марк", "Кот", grades4);

        Student[] students = {student1, student2, student3, student4};
        System.out.println(StudentService.bestStudent(students).getName());

        for (Student student: students
             ) {
            System.out.println(student.getSurname());
        }


    }
}
