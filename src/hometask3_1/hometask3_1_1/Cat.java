package hometask3_1.hometask3_1_1;

import java.util.Random;

public class Cat {
    Random random = new Random();

    Cat() {}

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        int i = random.nextInt(3);
        switch (i) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }
    }

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.status();
    }
}
