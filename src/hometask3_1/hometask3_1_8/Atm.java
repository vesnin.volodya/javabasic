package hometask3_1.hometask3_1_8;

public class Atm {
    private double rateDolToRub;
    private double rateRubToDol;
    private static int count = 0;

    Atm(double rateDolToRub, double rateRubToDol) {
        this.rateDolToRub = rateDolToRub;
        this.rateRubToDol = rateRubToDol;
        count++;
    }

    public double transferDollarsToRubles(double dollars) {
        return this.rateDolToRub * dollars;
    }

    public double transferRublesToDollars(double rubles) {
        return this.rateRubToDol * rubles;
    }

    public static int getCount() {
        return count;
    }




}
