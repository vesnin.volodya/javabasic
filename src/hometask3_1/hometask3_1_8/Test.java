package hometask3_1.hometask3_1_8;

public class Test {
    public static void main(String[] args) {
        Atm atm1 = new Atm(60.18, 0.016);

        System.out.println(atm1.transferDollarsToRubles(1500));
        System.out.println(atm1.transferRublesToDollars(5000));

        Atm atm2 = new Atm(60, 0.010);

        System.out.println(Atm.getCount());

    }
}
