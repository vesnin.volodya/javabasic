package hometask3_1.hometask3_1_4;

public class Test1 {
    public static void main(String[] args) {
        TimeUnit timeUnit = new TimeUnit(20, 30, 50);
        timeUnit.showTime();

        timeUnit.showTime12Format();

        timeUnit.addTime(23, 50, 30);
        timeUnit.showTime();
    }

}
