package hometask3_1.hometask3_1_4;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    TimeUnit(int hours, int minutes, int seconds){
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
    TimeUnit(int hours, int minutes){
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = 0;
    }
    TimeUnit(int hours){
        this.hours = hours;
        this.minutes = 0;
        this.seconds = 0;
    }


    public void showTime() {
        System.out.println(this.hours + ":" + this.minutes + ":" + this.seconds);
    }

    public void showTime12Format() {

        if (this.hours / 12.0 >= 1) {
            System.out.println((this.hours - 12) + ":" +
                    this.minutes + ":" + this.seconds + " pm" );
        } else {
            System.out.println(this.hours + ":" + this.minutes + ":" + this.seconds + " am");
        }
    }

    public void addTime(int hours, int minutes, int seconds) {
        this.seconds += seconds;
        this.minutes += minutes;
        this.hours += hours;

        if (this.seconds >= 60) {
            this.minutes += this.seconds / 60;
            this.seconds = this.seconds % 60;
        }

        if (this.minutes >= 60) {
            this.hours += this.minutes / 60;
            this.minutes = this.minutes % 60;
        }

        if (this.hours >= 24) {
            this.hours = this.hours % 24;
        }

    }


}
