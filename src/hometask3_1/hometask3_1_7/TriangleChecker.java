package hometask3_1.hometask3_1_7;

public class TriangleChecker {
    public static boolean check(double a, double b, double c) {
        return (a + b > c) && (a + c > b) && (b + c > a);
    }

    public static void main(String[] args) {
        System.out.println(TriangleChecker.check(3, 3, 3));
        System.out.println(TriangleChecker.check(10, 12, 3));

        System.out.println(TriangleChecker.check(10, 1, 3));

    }
}
