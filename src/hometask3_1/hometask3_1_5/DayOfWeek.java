package hometask3_1.hometask3_1_5;

public class DayOfWeek {
    private byte serialNumber;
    private String dayWeek;

    DayOfWeek(byte serialNumber, String dayWeek) {
        this.serialNumber = serialNumber;
        this.dayWeek = dayWeek;
    }

    public byte getSerialNumber() {
        return this.serialNumber;
    }

    public String getDayWeek() {
        return this.dayWeek;
    }
}
