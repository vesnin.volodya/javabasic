package firstweekpractice;

import java.util.Scanner;

/*
Дано целое число n.
Выведите следующее за ним четное число.
При решении этой задачи нельзя использовать условную инструкцию if и циклы.

5 -> 6
10 -> 12
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();

        int res = (num + 1) +  (num + 1) % 2;
        int res1 = (num / 2 +1) * 2;
        int res2 = num + 2 - Math.abs(num) % 2;

        System.out.printf("%s -> %s", num, res);
    }
}
