package secondweekpractice;
/*
Даны три целых числа a, b, c.
Проверить есть ли среди них прямо противоположные.
( 5 и -5 прямо противоположные числа).
0 и 0 не считать прямо противоположными.
Входные данные
-1 1 0
Выходные данные
true
Входные данные
-2 1 0
Выходные данные
false
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a, b, c;
        a = sc.nextInt();
        b = sc.nextInt();
        c = sc.nextInt();

        if (a == -1*b) {
            System.out.println(a != 0);
        } else if (a == -1*c) {
            System.out.println(a != 0);
        } else if (b == -1*c) {
            System.out.println(b != 0);
        } else {
            System.out.println(false);
        }

//        System.out.println((a == -b && a != 0) || (a == -c && a != 0) || (b == -c && b != 0));
    }
}
