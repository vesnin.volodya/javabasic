package secondweekpractice;

import java.util.Scanner;

/*
Дано число n.
Если оно четное и больше либо равно 0, то вывести “Четное больше или равно 0”.
Если четное и меньше 0, то вывести “Четное меньше 0”.
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        String str;
        if (n % 2 == 0) {
            if (n >= 0) {
                str = "Четное больше или равно 0";
            } else {
                str = "Четное меньше 0";
            }
            System.out.println(str);
        }


    }
}
