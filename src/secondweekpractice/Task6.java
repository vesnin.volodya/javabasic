package secondweekpractice;
/*
Даны три числа a, b, c.
Найти сумму двух чисел больших из них.
Входные данные
21 0 8
Выходные данные
29
 */

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b, c;
        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();

        int ab = a + b;
        int ac = a + c;
        int bc = b + c;
        if (ab >= ac && ab > bc) {
            System.out.println(ab);
        } else if (ac > ab && ac >= bc) {
            System.out.println(ac);
        } else {
            System.out.println(bc);
        }
        System.out.println(Math.max(Math.max(a + b, b + c), a +c));
    }
}
