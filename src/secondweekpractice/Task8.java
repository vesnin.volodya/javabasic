package secondweekpractice;

import java.util.Scanner;

/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char c = scanner.next().charAt(0);
//        if (symbol != Character.toUpperCase(symbol)) {
//            System.out.println(Character.toUpperCase(symbol));
//        } else {
//            System.out.println(Character.toLowerCase(symbol));
//        }

        if (c >= 'a' && c <= 'z') {
            System.out.println((char) (c + ('A' - 'a')));
        } else {
            System.out.println((char) (c - ('A' - 'a')));
        }
    }
}
