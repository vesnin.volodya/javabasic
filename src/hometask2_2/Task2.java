package hometask2_2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(); // количество столбцов и строк
        int[][] arr = new int[n][n];

        int x1 = sc.nextInt(); // координаты номера столбца x
        int y1 = sc.nextInt(); // координаты номера строки y

        int x2 = sc.nextInt(); // координаты номера столбца x
        int y2 = sc.nextInt(); // координаты номера строки y

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i == y1 || i == y2) && j >= x1 && j <= x2) {
                    arr[i][j] = 1;
                } else if (((j == x1 || j == x2) && i >= y1 && i <= y2)) {
                    arr[i][j] = 1;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n-1) {
                    System.out.print(arr[i][j]);
                } else System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
