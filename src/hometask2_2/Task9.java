package hometask2_2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        print(n);

    }

    public static void print(int n) {
        if (n>0) {
            print(n/10);
            System.out.print(n%10 + " ");
        }

    }
}
