package hometask2_2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[] arrNorma = new int[4];
        int[][] arrWeek = new int[7][4];

        int sumA = 0;
        int sumB = 0;
        int sumC = 0;
        int sumK = 0;

        for (int i = 0; i < arrNorma.length; i++) {
            arrNorma[i] = sc.nextInt();
        }

        for (int i = 0; i < arrWeek.length; i++) {
            for (int j = 0; j < arrWeek[i].length; j++) {
                arrWeek[i][j] = sc.nextInt();
            }
        }

        for (int i = 0; i < arrWeek.length; i++) {
            sumA += arrWeek[i][0];
            sumB += arrWeek[i][1];
            sumC += arrWeek[i][2];
            sumK += arrWeek[i][3];
        }

        if (sumA <= arrNorma[0] && sumB <= arrNorma[1] && sumC <= arrNorma[2] && sumK <= arrNorma[3]) {
            System.out.println("Отлично");
        } else System.out.println("Нужно есть поменьше");
    }
}
/*
882 595 1232 17500
142 85 76 2300
100 93 124 2500
282 70 144 3350
114 85 74 1900
96 77 60 1890
110 96 98 2500
155 67 124 3790
 */
