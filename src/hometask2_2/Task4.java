package hometask2_2;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(); // количество столбцов и строк
        int[][] arr1 = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr1[i][j] = sc.nextInt();
            }
        }

        int p = sc.nextInt();

        int[][] arr2 = new int[n-1][n-1];
        int findRow = 0;
        int findCol = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr1[i][j] == p) {
                    findCol = j;
                    findRow = i;
                }
            }
        }
        int k = 0;
        int l = 0;
        for (int i = 0; i < n; i++) {
            if (i!=findRow) {
                for (int j = 0; j < n; j++) {
                    if (j!=findCol) {
                        arr2[k][l] = arr1[i][j];
                        l++;
                    }
                }
                l=0;
                k++;
            }
        }

        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
                if (j == arr2[i].length-1) {
                    System.out.print(arr2[i][j]);
                } else System.out.print(arr2[i][j] + " ");
            }
            System.out.println();
        }


    }
}
