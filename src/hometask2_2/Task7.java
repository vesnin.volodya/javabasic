package hometask2_2;

import java.util.Scanner;
/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(); // количество строк
        String[] arrNamesPeople = new String[n];

        for (int i = 0; i < n; i++) {
            arrNamesPeople[i] = sc.next();
        }


        String[] arrNamesDogs = new String[n];

        for (int i = 0; i < n; i++) {
            arrNamesDogs[i] = sc.next();
        }

        int[][] arrScores = new int[n][3];
        double[] arrMeanScore = new double[n];

        for (int row = 0; row < arrScores.length; row++) {
            for (int col = 0; col < arrScores[row].length; col++) {
                arrScores[row][col] = sc.nextInt();
            }
        }

        for (int row = 0; row < arrScores.length; row++) {
            double mean = 0;
            int sum = 0;
            for (int col = 0; col < arrScores[row].length; col++) {
                sum += arrScores[row][col];
            }
            mean = sum / 3.0;
            arrMeanScore[row] = mean;
        }


        for (int i = 0; i < arrMeanScore.length-1; i++) {
            double currentMax = arrMeanScore[i];
            String currentMaxPeople = arrNamesPeople[i];
            String currentMaxDogs = arrNamesDogs[i];
            int currentMaxIndex = i;
            for (int j = i+1; j < arrMeanScore.length; j++) {
                if (arrMeanScore[j] > currentMax) {
                    currentMax = arrMeanScore[j];
                    currentMaxPeople = arrNamesPeople[j];
                    currentMaxDogs = arrNamesDogs[j];
                    currentMaxIndex = j;
                }
            }
            if (currentMaxIndex != i) {
                arrMeanScore[currentMaxIndex] = arrMeanScore[i];
                arrNamesPeople[currentMaxIndex] = arrNamesPeople[i];
                arrNamesDogs[currentMaxIndex] = arrNamesDogs[i];
                arrMeanScore[i] = currentMax;
                arrNamesPeople[i] = currentMaxPeople;
                arrNamesDogs[i] = currentMaxDogs;
            }

        }
        for (int i = 0; i < 3; i++) {
            System.out.println(arrNamesPeople[i] + ": " + arrNamesDogs[i] + ", " + (int)(arrMeanScore[i]*10) / 10.0);
            System.out.println(arrNamesDogs[i]);
        }


    }
}
