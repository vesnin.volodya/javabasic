package hometask2_2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(); // количество столбцов и строк
        int[][] arr1 = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr1[i][j] = sc.nextInt();
            }
        }
        boolean isSym = false;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i==j) {
                    if (arr1[i][j] == arr1[n-1-i][n-1-j]) {
                        isSym = true;
                    } else isSym = false;
                }
            }
        }
        System.out.println(isSym);
    }
}
