package hometask2_2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(); // количество столбцов и строк
        char[][] arr = new char[n][n];

        int x = sc.nextInt(); // координаты номера столбца x
        int y = sc.nextInt(); // координаты номера строки y

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == y && j == x) {
                    arr[i][j] = 'K';
                } else if ((i == y-1 || i == y+1)
                        && (j == x + 2 || j == x - 2)) {
                    arr[i][j] = 'X';
                } else if ((i == y-2 || i == y+2)
                        && (j == x + 1 || j == x - 1)) {
                    arr[i][j] = 'X';
                } else arr[i][j] = '0';
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n-1) {
                    System.out.print(arr[i][j]);
                } else System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
