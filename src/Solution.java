import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        String[] arr1 = new String[n];

        for (int i=0; i < n; i++) {
            arr1[i] = sc.next();
        }

        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                if (arr1[i].equals(arr1[j])) {
                    System.out.println(arr1[j]);
                    break;
                }
            }
        }
    }
}
