import java.util.Arrays;
import java.util.Scanner;

public class ConvertAmountWithSelection {
  static final double ROUBLES_PER_Dollar = 72.12;

  public static void main(String[] args) {
    int[] dollarsArray;
    double[] roublesArray;
    int digit;
    int n;
    int i;

    Scanner sc = new Scanner(System.in);

    // Отобразить инструкцию
    instruct();

    // Получить количество конвертаций
    do {
      System.out.println("Введите корректное количество конвертаций: ");
      n = sc.nextInt();
    } while (n <= 0);


    // Получить сумму денег в евро;
    System.out.print("Введите " + n + " сумм денег в американских долларах: ");
    dollarsArray = new int[n];
    for (i=0; i<n; i++) {
      dollarsArray[i] = sc.nextInt();
    }

    // Конвертировать n сумму денег в российские рубли
    roublesArray = find_roubles(dollarsArray, n);

    //Отобразить сумму американских долларов с правильным окончанием
    System.out.println(Arrays.toString(dollarsArray));

//    if (5 <= dollars && dollars <= 20) {
//      System.out.print(" американских долларов равны ");
//    } else {
//      digit = dollars % 10;
//
//      if (digit == 1) {
//        System.out.print(" американский доллар равен ");
//      } else if (2 <= digit && digit <= 4) {
//        System.out.print(" американских доллара равны ");
//      } else {
//        System.out.print(" американских долларов равны ");
//      }
//    }

    // Вывести сумму в рублях
    for (i=0; i<n; i++) {
      System.out.println((int)(roublesArray[i] * 100) / 100.0 +
              " рублей");
    }



  }

  private static double[] find_roubles(int[] dollarsArray, int n) {
    double[] rublesArray = new double[n];
    int i;
    for (i=0; i<n; i++) {
      rublesArray[i] = dollarsArray[i] * ROUBLES_PER_Dollar;
    }
    return rublesArray;
  }

  /*
  Отображает инструкцию
   */
  public static void instruct() {
    System.out.println("Это программа конверитрует сумму денег "
            + "из американских долларов в российские рубли.");
    System.out.println("Курс покупки равен " + ROUBLES_PER_Dollar + " рубля.\n");
  }
  
}