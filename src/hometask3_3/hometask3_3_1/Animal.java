package hometask3_3.hometask3_3_1;

public abstract class Animal {

    abstract public void wayOfBirth();

    final public void eat() {
        System.out.println("Ест");
    }

    final public void sleep() {
        System.out.println("Спит");
    }
}