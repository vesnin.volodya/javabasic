package hometask3_3.hometask3_3_1;

public class Bird extends Animal implements Flying{


    @Override
    public void wayOfBirth() {
        System.out.println("Откладывают яйца");
    }

    @Override
    public void fly() {
        System.out.println("Летит");
    }
}
