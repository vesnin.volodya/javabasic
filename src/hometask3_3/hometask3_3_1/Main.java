package hometask3_3.hometask3_3_1;

public class Main {
    public static void main(String[] args) {
        GoldFish goldFish = new GoldFish();
        Eagle eagle = new Eagle();

        goldFish.wayOfBirth();
        goldFish.swim();

        eagle.wayOfBirth();
        eagle.fly();

        eagle.eat();
        goldFish.sleep();

    }
}
