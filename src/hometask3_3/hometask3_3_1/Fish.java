package hometask3_3.hometask3_3_1;

public class Fish extends Animal implements Swimming{

    @Override
    public void wayOfBirth() {
        System.out.println("Мечут икру");
    }

    @Override
    public void swim() {
        System.out.println("Плывет");
    }
}
