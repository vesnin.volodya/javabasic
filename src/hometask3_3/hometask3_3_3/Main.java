package hometask3_3.hometask3_3_3;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        ArrayList<ArrayList<Integer>> matrix = getMatrix(n, m);

        printMatrix(matrix);

    }

    public static ArrayList<ArrayList<Integer>> getMatrix(int n, int m) {
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();

        for (int i = 0; i < m; i++) {
            ArrayList<Integer> row = new ArrayList<Integer>();
            for (int j = 0; j < n; j++) {
                row.add(i+j);
            }
            matrix.add(row);
        }

        return matrix;
    }
    public static void printMatrix(ArrayList<ArrayList<Integer>> matrix) {
        for (ArrayList<Integer> row: matrix
        ) {
            for (Integer element: row
            ) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }
}
