package hometask3_3.hometask3_3_2;

public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        Stool stool = new Stool();
        Table table = new Table();

        System.out.println(bestCarpenterEver.canFix(stool));
        System.out.println(bestCarpenterEver.canFix(table));
    }
}
