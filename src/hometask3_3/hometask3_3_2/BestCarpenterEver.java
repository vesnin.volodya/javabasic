package hometask3_3.hometask3_3_2;

public class BestCarpenterEver {
    public boolean canFix(Furniture furniture) {
        return furniture instanceof Stool;
    }
}
