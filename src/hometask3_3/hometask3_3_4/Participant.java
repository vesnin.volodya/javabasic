package hometask3_3.hometask3_3_4;

public class Participant implements Comparable<Participant> {
    private String name;
    private Dog dog;

    Participant(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public String getName() {
        return this.name;
    }

    public String getDogName() {
        return this.dog.getName();
    }

    public double getMeanScore() {
        return dog.getMeanScore();
    }

    @Override // Реализует метод compareTo, опеределенный в Comparable
    public int compareTo(Participant o) {
        if (getMeanScore() > o.getMeanScore())
            return -1;
        else if (getMeanScore() < o.getMeanScore())
            return 1;
        else
            return 0;
    }


}
