package hometask3_3.hometask3_3_4;

public class Dog {
    private String name;
    private int[] scores = new int[3];;

    private double meanScore;

    Dog(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    public void calcMeanScore() {
        int sum = 0;
        for (int score: scores
        ) {
            sum += score;
        }
        meanScore = sum / 3.0;
    }
    public double getMeanScore() {
        return meanScore;
    }

}
