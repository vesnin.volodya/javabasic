package hometask3_3.hometask3_3_4;

import java.util.ArrayList;
import java.util.Scanner;

/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
 */

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        ArrayList<Participant> arrParticipants = new ArrayList<Participant>();

        for (int i = 0; i < n; i++) {
            arrParticipants.add(new Participant(sc.next()));
        }

        ArrayList<Dog> arrDogs = new ArrayList<Dog>();

        for (int i = 0; i < n; i++) {
            Dog dog = new Dog(sc.next());
            arrDogs.add(dog);
            arrParticipants.get(i).setDog(dog);
        }

        for (Dog dog: arrDogs
             ) {
            int[] scores = new int[3];
            for (int i = 0; i < 3; i++) {
                 scores[i] = sc.nextInt();
            }
            dog.setScores(scores);
            dog.calcMeanScore();
        }

        arrParticipants.sort(Participant::compareTo);

        for (int i = 0; i < 3; i++) {
            System.out.println(arrParticipants.get(i).getName() + ": " + arrParticipants.get(i).getDogName() + ", " + (int)(arrParticipants.get(i).getMeanScore()*10) / 10.0);
        }
    }

}
