package professional.hometask1.task6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Person {
    private String name;
    private String birthdate;
    private String gender;
    private String height;

    Person(String name, String birthdate, String gender, String height) {

        try {
            checkName(name);
            this.name = name;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try {
            checkBirthdate(birthdate);
            this.birthdate = birthdate;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try {
            checkGender(gender);
            this.gender = gender;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try {
            checkHeight(height);
            this.height = height;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void checkName(String str) throws Exception {
        if ((str.length() < 2 || str.length() > 20) || (str.charAt(0) < 'A' || str.charAt(0) > 'Z')) {
            throw new Exception("Длина имени должна быть от 2 до 20 символов, первая буква заглавная");
        }
    }

    public void checkBirthdate(String str) throws Exception{
        LocalDate date = LocalDate.parse(str, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        LocalDate minDate = LocalDate.parse("01.01.1900", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        LocalDate maxDate = LocalDate.now();

        if (date.isBefore(minDate) || date.isAfter(maxDate) ) {
            throw new Exception("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
        }
    }

    public void checkGender(String str) throws Exception {
        if (!Gender.Male.toString().equals(str) && !Gender.Female.toString().equals(str)) {
            throw new Exception("некорректный пол");
        }
    }

    public void checkHeight(String str) throws Exception {
        double height;
        try {
            height = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            throw new Exception(e);
        }

        if (height < 0) {
            throw new Exception("Рост должен быть положительным числом");
        }
    }
}
