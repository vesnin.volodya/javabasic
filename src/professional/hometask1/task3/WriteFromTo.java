package professional.hometask1.task3;

import java.io.*;
import java.util.Arrays;
import java.util.Locale;

public class WriteFromTo {
    private String fromFilePath;
    private String toFilePath;
    private String text;

    WriteFromTo(String from, String to) {
        this.fromFilePath = from;
        this.toFilePath = to;
        this.text = read();
    }

    private String read() {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fromFilePath))) {
            String sCurrentLine;
            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                contentBuilder.append(sCurrentLine).append("\n");
            }
            bufferedReader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return contentBuilder.toString();
    }

    public String getText() {
        return this.text;
    }

    public String getTextWithUpperCase() {
        return this.text.toUpperCase(Locale.ROOT);
    }

    public void rewriteWithUpperCase() {

        try (FileWriter fileWriter = new FileWriter(toFilePath)) {
            String textWithUpperCase = getTextWithUpperCase();
            fileWriter.write(textWithUpperCase);
            fileWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }


}
