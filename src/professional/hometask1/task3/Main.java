package professional.hometask1.task3;

public class Main {
    public static void main(String[] args) {
        WriteFromTo writeFromTo = new WriteFromTo("./src/professional/hometask1/task3/input.txt",
                "./src/professional/hometask1/task3/output.txt");
        System.out.println(writeFromTo.getTextWithUpperCase());
        writeFromTo.rewriteWithUpperCase();
    }
}
