package professional.hometask1.task4;

public class MyEvenNumber{
    private final int n;

    MyEvenNumber(int number) throws MyNumberException {
        if (number % 2 == 0) {
            this.n = number;
        } else throw new MyNumberException("Число должно быть четным");

    }


}
