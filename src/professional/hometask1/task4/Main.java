package professional.hometask1.task4;

public class Main {
    public static void main(String[] args) {
        try {
            MyPrimeNumber myPrimeNumber1 = new MyPrimeNumber(3);
        } catch (MyNumberException ex) {
            System.out.println(ex);
        }

        try {
            MyPrimeNumber myPrimeNumber2 = new MyPrimeNumber(2);
        } catch (MyNumberException ex) {
            System.out.println(ex);
        }

    }
}
