package professional.hometask1.task4;

public class MyNumberException extends RuntimeException{
    MyNumberException(String msg) {
        super(msg);
    }
}
