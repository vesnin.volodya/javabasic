package professional.hometask1.task1;

public class Main {
    public static void main(String[] args) {
        try {
            myFunction(-2);
        } catch (MyCheckedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void myFunction(int number) throws MyCheckedException {
        if (number >= 0) {
            System.out.println("Work!");
        } else throw new MyCheckedException("Error");
    }
}
