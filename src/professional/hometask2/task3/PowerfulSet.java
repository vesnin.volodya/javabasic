package professional.hometask2.task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {



    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> resSet = new HashSet<>(set1);
        resSet.retainAll(set2);

        return resSet;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> resSet = new HashSet<>(set1);
        resSet.addAll(set2);
        return resSet;
    }

    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> resSet = new HashSet<>(set1);
        resSet.removeAll(set2);
        return resSet;
    }
}
