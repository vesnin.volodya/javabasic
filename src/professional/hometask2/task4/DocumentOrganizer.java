package professional.hometask2.task4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class DocumentOrganizer {
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> documentMap = new HashMap<>();

        for (Document document: documents
             ) {
            documentMap.put(document.id, document);
        }
        return documentMap;
    }
}
