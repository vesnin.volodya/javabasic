package professional.hometask2.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Document doc1 = new Document(1, "Doc1", 45);
        Document doc2 = new Document(2, "Doc2", 65);
        Document doc3 = new Document(3, "Doc3", 155);

        List<Document> documentList = Arrays.asList(doc1, doc2, doc3);


        Map<Integer, Document> myDocMap = DocumentOrganizer.organizeDocuments(documentList);

        System.out.println(myDocMap);
    }
}
