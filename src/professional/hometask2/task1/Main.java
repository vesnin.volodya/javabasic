package professional.hometask2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> arr1 = new ArrayList<>();
        arr1.add("cat");
        arr1.add("dog");
        arr1.add("cat");
        arr1.add("fish");
        arr1.add("dog");

        ArrayList<String> uniqueArr1 = getUniqueElements(arr1);
        System.out.println(uniqueArr1);

        System.out.println(convert(arr1));
    }

    public static <T> ArrayList<T> getUniqueElements(ArrayList<T> arrayList) {
        ArrayList<T> uniqueArr = new ArrayList<T>();
        uniqueArr.add(arrayList.get(0));
        for (int i = 1; i < arrayList.size(); i++) {
            if (!uniqueArr.contains(arrayList.get(i))) {
                uniqueArr.add(arrayList.get(i));
            }
        }
        return uniqueArr;
    }

    public static <T> HashSet<T> convert(ArrayList<T> from) {
        return new HashSet<>(from);
    }
}
