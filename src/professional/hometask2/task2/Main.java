package professional.hometask2.task2;

import javax.annotation.processing.SupportedSourceVersion;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String s = userEnter(); //listen
        String v = userEnter(); //silent

        System.out.println(isAnagramm(s, v));
    }

    public static boolean isAnagramm(String s, String v) {
        boolean boolIsAnagram = false;
        ArrayList<Character> sArray = sortArrayList(s);
        ArrayList<Character> vArray = sortArrayList(v);

//        System.out.println(sArray);
//        System.out.println(vArray);

        if (sArray.size() == vArray.size()) {
            for (int i = 0; i < sArray.size(); i++) {
                if (sArray.get(i) != vArray.get(i)) {
                    boolIsAnagram = false;
                    break;
                } else boolIsAnagram = true;
            }
        }

        return boolIsAnagram;
    }

    private static ArrayList<Character> sortArrayList(String s) {
        ArrayList<Character> characterArrayList = new ArrayList<Character>();
        for (char c : s.toLowerCase().toCharArray()) {
            characterArrayList.add(c);
        }
        characterArrayList.sort(Character::compareTo);
        return characterArrayList;
    }

    public static String userEnter() {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        return str;
    }


}
