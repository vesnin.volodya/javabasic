package professional.hometask3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {
        Class<APrinter> cls = APrinter.class;
        try {
            Method method = cls.getMethod("print", int.class);
            method.invoke(APrinter.class, 8);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
