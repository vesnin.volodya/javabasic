package professional.hometask3.task1_2;

public class Main {
    public static void main(String[] args) {
        check(LikeClass.class);
        check(NotLikeClass.class);
    }

    public static void check(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            System.out.println("No IsLike annotation");
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("IsLike value: " + isLike.value());
    }
}
