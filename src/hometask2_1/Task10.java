package hometask2_1;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        game();
    }
    public static int thinkNumber(int minValue, int maxValue) {
        return (int) ((Math.random() * (maxValue - minValue)) + minValue);
    }

    public static int guessNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите ваше число: ");
        return sc.nextInt();
    }

    public static void game() {
        int num = thinkNumber(0, 1000);
        System.out.println("Угадайте загаданное число!");
        while (true) {
            int guessNum = guessNumber();
            if (num == guessNum) {
                System.out.println("Победа!");
                break;
            } else if (num > guessNum) {
                System.out.println("Это число меньше загаданного");
            } else {
                System.out.println("Это число больше загаданного");
            }
        }
    }

}
