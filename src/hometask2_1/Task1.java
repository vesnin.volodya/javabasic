package hometask2_1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        double[] a = new double[n];
        for (int i=0; i < n; i++) {
            a[i] = sc.nextDouble();
        }

        System.out.println(mean(a));

    }

    public static double mean(double[] arr) {
        double meanNum = 0;
        for (int i=0; i < arr.length; i++) {
            meanNum += arr[i];
        }
        return meanNum / arr.length;
    }
}
