package hometask2_1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++) {
            arr1[i] = sc.nextInt();
        }

        int m = sc.nextInt();
        int[] arr2 = new int[m];
        for (int i=0; i < n; i++) {
            arr2[i] = sc.nextInt();
        }

        Boolean isEqual = isEqualArr(arr1, arr2);

        System.out.println(isEqual);
    }

    public static Boolean isEqualArr(int[] arr1, int[] arr2) {
        Boolean isEqual = false;
        if (arr1.length == arr2.length) {
            for (int i=0; i < arr1.length; i++) {
                if (arr1[i] == arr2[i]) {
                    isEqual = true;
                } else {
                    isEqual = false;
                    break;
                }
            }
        } else isEqual = false;

        return isEqual;
    }
}
