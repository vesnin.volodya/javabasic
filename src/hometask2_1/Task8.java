package hometask2_1;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr1 = new int[n];

        for (int i=0; i < n; i++) {
            arr1[i] = sc.nextInt();
        }

        int m = sc.nextInt();
        int dist;
        int minDist = Math.abs(arr1[0]-m);;
        int minDistElement = arr1[0];
        for (int i = 1; i < n; i++) {
            dist = Math.abs(arr1[i]-m);
            if (dist <= minDist && arr1[i] > minDistElement) {
                minDist = dist;
                minDistElement = arr1[i];
            }
        }
        System.out.println(minDistElement);


    }
}
