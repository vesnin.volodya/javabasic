package hometask2_1;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr1 = new int[n];
        for (int i=0; i < n; i++) {
            arr1[i] = sc.nextInt();
        }

        int x = sc.nextInt();
        int index = 0;
        for (int i=0; i < arr1.length; i++) {
            if (arr1[i] <= x && arr1[i+1] >= x) {
                index = i+1;
            }
        }
        System.out.println(index);
    }
}
