package hometask2_1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        String[] MORZE = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        char[] inputArray = input.toCharArray();

        for (int i = 0; i < inputArray.length; i++) {

            System.out.print(MORZE[(int)inputArray[i] - (int)'А'] + " ");
        }
    }
}
