package hometask2_1;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr1 = new int[n];

        for (int i=0; i < n; i++) {
            arr1[i] = sc.nextInt();
        }

        int shift = sc.nextInt();

        int[] arr2 = new int[n];
        for (int i = 0; i < n; i++) {
            if (n <= i + shift) {
                arr2[i+shift-n] = arr1[i];
            } else {
                arr2[i+shift] = arr1[i];
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arr2[i] + " ");
        }
    }
}
