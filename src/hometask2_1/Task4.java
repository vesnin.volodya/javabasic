package hometask2_1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr1 = new int[n];

        for (int i=0; i < n; i++) {
            arr1[i] = sc.nextInt();
        }

        int count = 0;
        int lastNum = arr1[0];

        for (int i=0; i<arr1.length; i++) {
            if (arr1[i] == lastNum) {
                count++;

            } else {
                System.out.println(count + " " + lastNum);
                count = 1;

            }
            lastNum = arr1[i];
            if (i == arr1.length-1) {
                System.out.println(count + " " + lastNum);
                break;
            }
        }
    }
}
